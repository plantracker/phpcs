<?php

namespace PlanTracker;

use PhpCsFixer\Config;
use PhpCsFixer\Finder;

function styles(Finder $finder, array $rules = []): Config {
    $config = new Config;
    $rules = array_merge(require __DIR__ . '/rules.php', $rules);

    return $config
        ->setFinder($finder)
        ->setRiskyAllowed(true)
        ->setRules($rules);
}
